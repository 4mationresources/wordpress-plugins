WordPress Plugins
===================

This is the repository that 4mation should store all of its publically available plugins in. All plugins should be in their zip format so that composer can download them properly. 

Adding a plugin
-------------
Simply upload the plugin in the Downloads page. 

Composer Setup
-------------

To make use of the the plugins in this repo, you need to setup the `repository` within composer so that it can resolve the file correctly. This will typically look like.

```
  "repositories": {
  ....
 {
      "type": "package",
      "package": {
        "name": "4m-mirror/gravity-forms",
        "version": "2.3.3.10",
        "type": "wordpress-plugin",
        "dist": {
          "type": "zip",
          "url": "hhttps://bitbucket.org/4mationresources/wordpress-plugins/downloads/gravityforms_2.3.3.10.zip"
        }
      }
    }
	}
```

Then within your `require`

`"4m-mirror/gravity-forms": "^2.3.3.10",`